{application,ecoelixir,
             [{applications,[kernel,stdlib,elixir,logger,math]},
              {description,"ecoelixir"},
              {modules,['Elixir.Diversity']},
              {registered,[]},
              {vsn,"0.1.0"},
              {extra_applications,[logger]}]}.
